const addMovieModal = document.getElementById('add-modal');
const startAddMovieBtn = document.querySelector('header button');
const backdrop = document.getElementById('backdrop');
const cancelAddMovieBtn = addMovieModal.querySelector('.btn--passive');
const confirmAddMovieBtn = cancelAddMovieBtn.nextElementSibling;
const userInputs = addMovieModal.querySelectorAll('input');
const entryText = document.getElementById('entry-text');

const movies = [];

const updateUI = () => {
    if (movies.length === 0) {
        entryText.style.display = 'block';
    } else {
        entryText.style.display = 'none';
    }
};
const deleteMovie = (movieId) => {
    let movieIndex = 0;
    for (const movie of movies) {
        if (movie.id === movieId) {
            break;
        }
        movieIndex++;
    }
    movies.splice(movieIndex, 1);
    const listRoot = document.getElementById('movie-list');
    listRoot.children[movieIndex].remove();
}
const removeMovie = (movieId) => {

    const deleteModal = document.getElementById('delete-modal');
    deleteModal.classList.add('visible');
    toggleBackdrop();

    deleteMovie(movieId);
   
};
const renderNewMovie = ({id, title, imageUrl, rating}) => {
    const newMovie = document.createElement('li');
    newMovie.classList.add('movie-element');
    newMovie.innerHTML = `
        <div class='movie-element__image'>
            <img src='${imageUrl}' alt='${title}'/>
        </div>
        <div class='movie-element__info'>
            <h2>${title}</h2>
            <p>${rating}/5 stars</p>
        </div>
    `;
    newMovie.addEventListener('click', removeMovie.bind(null, id));
    const listRoot = document.getElementById('movie-list');
    listRoot.append(newMovie);
};
const toggleBackdrop = () => {
    backdrop.classList.toggle('visible');
};
const toggleMovieModal = () => {
    addMovieModal.classList.add('visible');
    toggleBackdrop();
};

const quitMovieModal = () => {
    addMovieModal.classList.remove('visible')
}

const closeMovieModal = () => {
    toggleMovieModal();
};
const clearMovieInputs = () => {
    for (const input of userInputs) {
        input.value = '';
    }
};
const cancelAddMovie = () => {
    quitMovieModal();
    clearMovieInputs();
};
const addMovie = () => {
    const title = userInputs[0].value.trim();
    const imageUrl = userInputs[1].value.trim();
    const rating = userInputs[2].value.trim();

    if (!title.trim() || !imageUrl.trim() || !rating.trim() || +rating < 1 || +rating > 5) {
        alert('Please enter the valid input!');
    }
    const newMovie = { id: Math.random().toString(), title, rating, imageUrl };
    movies.push(newMovie);
    quitMovieModal();
    renderNewMovie(newMovie);
    updateUI();
    clearMovieInputs();
};


startAddMovieBtn.addEventListener('click', toggleMovieModal);
backdrop.addEventListener('click', quitMovieModal);
cancelAddMovieBtn.addEventListener('click', cancelAddMovie);
confirmAddMovieBtn.addEventListener('click', addMovie);